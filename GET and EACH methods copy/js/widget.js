//
////// JQUERY USING $.get AND $.each METHODS //////
//
//
// FIRST STEP IF <script> TAG IS IN HEADER
//END .ready
//
// ADD JQUERY METHOD
//END .getJSON
//END .ready
//
// SET VARIABLES
//END .getJSON
//END .ready
//
// ADD .each METHOD 
$(document).ready(function() {
  var url = '../data/employees.json';
  $.getJSON(url,function (servRes) {
    var employeeStatusHTML = '<ul class="bulleted">';
    $.each(servRes, function(index, employee) { // index = ARRAY ITEM NUMBER | employee = ARRAY VALUE //VARIABLE
      if (employee.inoffice === true) {
        employeeStatusHTML += '<li class = "in">';
      } else {
        employeeStatusHTML += '<li class = "out">';
      }
      employeeStatusHTML += employee.name + '</li>';// END if STATEMENT
    });//END .each 
    employeeStatusHTML += '</ul>';
    $('#employeeList').html(employeeStatusHTML);
  });//END .getJSON
});//END .ready
//
// 